jQuery(document).ready(function () {

    //Wow
    wow = new WOW({

        mobile: false // default
    })
    wow.init();


    //Sticky Header
    jQuery(window).scroll(function () {
        var scroll = $(window).scrollTop();

        if (scroll >= 200) {
            jQuery(".header").addClass("fixed-header");
        } else {
            jQuery(".header").removeClass("fixed-header");
        }
    });
    
    // Gallery Slider
    
    jQuery('.adv-img-sec1').slick({
      dots: false,
      infinite: false,
      speed: 300,
      slidesToShow: 1,
      slidesToScroll: 1,
      arrows: true,
      prevArrow: '<a class="slick-prev"><i class="fa fa-angle-left" aria-hidden="true"></i></a>',
      nextArrow: '<a class="slick-next"><i class="fa fa-angle-right" aria-hidden="true"></i></a>',
    });
    
    // Grid Slider
    
    jQuery('.grid-slider').slick({
      dots: false,
      infinite: false,
      speed: 300,
      slidesToShow: 1,
      slidesToScroll: 1,
      arrows: true,
      prevArrow: '<a class="slick-prev"><i class="fa fa-angle-left" aria-hidden="true"></i></a>',
      nextArrow: '<a class="slick-next"><i class="fa fa-angle-right" aria-hidden="true"></i></a>',
    });
    
    // Gallery //

   var filterList = {
        init: function () {
            // MixItUp plugin
            // http://mixitup.io
            jQuery('#portfoliolist').mixItUp({
                selectors: {
                    target: '.portfolio',
                    filter: '.filter'
                },
                load: {
                    filter: '.jockey-apparel, .goggles, .boots'
                }
            });
        }
    };
    // Run the show!
    filterList.init();
    
});

//Textarea 

jQuery(function (jQuery) {
    jQuery('.firstCap').on('keypress', function (event) {
        var $this = $(this),
            thisVal = $this.val(),
            FLC = thisVal.slice(0, 1).toUpperCase();
        con = thisVal.slice(1, thisVal.length);
        jQuery(this).val(FLC + con);
    });
});

// Faq Section

function toggleIcon(e) {
        jQuery(e.target)
            .prev('.panel-heading')
            .find(".more-less")
            .toggleClass('fa fa-plus fa fa-minus');
    }
    jQuery('.panel-group').on('hidden.bs.collapse', toggleIcon);
    jQuery('.panel-group').on('shown.bs.collapse', toggleIcon);

//Page Zoom

document.documentElement.addEventListener('touchstart', function (event) {
 if (event.touches.length > 1) {
   event.preventDefault();
 }
}, false);

// Upload Section
jQuery(document).on('change', '.btn-file :file', function() {
  var input = $(this),
      numFiles = input.get(0).files ? input.get(0).files.length : 1,
      label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
  input.trigger('fileselect', [numFiles, label]);
});

jQuery(document).ready( function() {
    jQuery('.btn-file :file').on('fileselect', function(event, numFiles, label) {
        
        var input = $(this).parents('.input-group').find(':text'),
            log = numFiles > 1 ? numFiles + ' files selected' : label;
        
        if( input.length ) {
            input.val(log);
        } else {
            if( log ) alert(log);
        }
        
    });
});